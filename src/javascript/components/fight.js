import { controls } from '../../constants/controls';

let isFirstPlayerHasBlock = 0;
let isSecondPlayerHasBlock = 0;

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    console.log(firstFighter);
    console.log(secondFighter);

    let firstFighterTotalDamage = 0;
    let secondFighterTotalDamage = 0;
    let firstFighterBlockValue = 0;
    let secondFighterBlockValue = 0;

    let leftFighterHealthBar = document.querySelector('#left-fighter-indicator');
    let rightFighterHealthBar = document.querySelector('#right-fighter-indicator ');

    
    window.addEventListener("keydown", function (event) { 
      if (event.defaultPrevented) {
        return;
      }
      switch (event.key.toLowerCase()) {
        case 'd':
          isFirstPlayerHasBlock = 1;
          break;
        case 'l':
          isSecondPlayerHasBlock = 1;
          break;
      }
    });
    window.addEventListener("keyup", function (event) {
      if (event.defaultPrevented) {
        return;
      }
      switch (event.key.toLowerCase()) {
        case 'd' : 
          isFirstPlayerHasBlock = 0;
          firstFighterBlockValue = 0;
          break;
        case 'l':
          isSecondPlayerHasBlock = 0;
          secondFighterBlockValue = 0;
          break; 
        case 'a' :
          if(isFirstPlayerHasBlock) {
            break;
          }
          if(isSecondPlayerHasBlock) {
            break;
          }
          let damageForSecondFighter = getDamage(firstFighter, secondFighter);
          secondFighterTotalDamage += damageForSecondFighter;
          secondFighter.health = secondFighter.health - damageForSecondFighter;

          let remainingSecondFighterHealthInPercent = secondFighter.health > 0 ? secondFighter.health / (secondFighter.health + secondFighterTotalDamage) * 100 : 0;
          rightFighterHealthBar.style.width = `${remainingSecondFighterHealthInPercent}%`;
          break;
        case 'j' :
          if(isSecondPlayerHasBlock) {
            break;
          }
          if(isFirstPlayerHasBlock){
            break;
          }
          let damageForFirstFighter = getDamage(secondFighter, firstFighter);
          firstFighterTotalDamage += damageForFirstFighter;
          firstFighter.health = firstFighter.health - damageForFirstFighter;

          let remainingHealthInPercent = firstFighter.health > 0 ? firstFighter.health / (firstFighter.health + firstFighterTotalDamage) * 100 : 0;
          leftFighterHealthBar.style.width = `${remainingHealthInPercent}%`;
          break;
      }

      if(firstFighter.health <= 0){
        leftFighterHealthBar.style.width = 0;

        return resolve(secondFighter);
      }

      if(secondFighter.health <= 0){
        rightFighterHealthBar.style.width = 0;

        return resolve(firstFighter);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let givenDamage = getHitPower(attacker) - getBlockPower(defender);
  if(givenDamage < 0) {
    givenDamage = 0;
  }
  console.log(givenDamage);

  return givenDamage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.floor(Math.random() * 2) + 1;
  console.log('attk: ' + criticalHitChance);
  let power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.floor(Math.random() * 2) + 1;
  let power = fighter.defense * dodgeChance;
  console.log('def power: ' + power);

  return power;
}
