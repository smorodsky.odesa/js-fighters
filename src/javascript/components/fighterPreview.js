import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';

  let fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });
  let fighterInfoWrapper = createElement({tagName: 'div', className: `figher-info figher-params-wrapper`});
  let fighterImgWrapper = createElement({tagName: 'div', className: `figher-info figher-info-img-preview-wrapper`});
  try{
    for(let fighterData in fighter){
      if(fighterData == '_id') {
        continue;
      }
      if(fighterData == 'source') {
        let figherImg = createFighterImage(fighter);

        fighterImgWrapper.append(figherImg);
        fighterElement.append(fighterImgWrapper);
        continue;
      }
      let fighterParam = createElement({
        tagName: 'div',
        className: `fighter-data-preview fighter-preview___${fighterData}`
      });
      fighterParam.innerHTML = `<span>${fighterData}</span>: ${fighter[fighterData]}`;
      fighterInfoWrapper.append(fighterParam);
    }
    
    fighterElement.append(fighterInfoWrapper);
  } catch(error){
    console.log(error);
  }
  return fighterElement;

}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
